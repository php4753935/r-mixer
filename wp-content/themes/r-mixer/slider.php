<section id="slider">
        <div class="slider-content">
            <!-- Get post News Query -->
            <?php $getposts = new WP_query(); $getposts->query('post_status=publish&showposts=4&post_type=slider'); ?>
            <?php global $wp_query; $wp_query->in_the_loop = true; ?>
            <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                <a href="<?=the_permalink();?>" class="image-item">
                    <div class="image">
                        <img src="<?php echo $featured_img_url; ?>" alt="ảnh1" />
                    </div>
                    <div class="con">
                        <h3 class="image-title"><?php the_title(); ?></h3>
                        <?php the_excerpt(); ?>
                    </div>
                </a>
            <?php endwhile; ?>
            <!-- Get post News Query -->



            <!-- <div class="image-item">
                <div class="image">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh1.png" alt="ảnh1" />
                </div>
                <div class="con">
                    <h3 class="image-title">bicolo 尾山台【ペットと暮らせる</h3>
                    <p class="image-desc">対比的な２つの場所 <br />世田谷区等々力二丁目</p>
                </div>
            </div>
            <div class="image-item">
                <div class="image">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh2.png" alt="ảnh2" />
                </div>
                <div class="con">
                    <h3 class="image-title">辰巳アパートメントハウス</h3>
                    <p class="image-desc">
                        design by 伊藤博之建築設計事務所 <br />東京都江東区門前仲町1丁目
                    </p>
                </div>
            </div>
            <div class="image-item">
                <div class="image">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh3.png" alt="ảnh3" />
                </div>
                <div class="con">
                    <h3 class="image-title">北千住park view（シェアハウス）</h3>
                    <p class="image-desc">
                        遊び心＋高品質設備により、暮らしの豊かさを味わって欲しい
                        <br />東京都足立区千住龍田町
                    </p>
                </div>
            </div>
            <div class="image-item">
                <div class="image">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh3.png" alt="ảnh3" />
                </div>
                <div class="con">
                    <h3 class="image-title">北千住park view（シェアハウス）</h3>
                    <p class="image-desc">
                        遊び心＋高品質設備により、暮らしの豊かさを味わって欲しい
                        <br />東京都足立区千住龍田町
                    </p>
                </div>
            </div>
            <div class="image-item">
                <div class="image">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh3.png" alt="ảnh3" />
                </div>
                <div class="con">
                    <h3 class="image-title">北千住park view（シェアハウス）</h3>
                    <p class="image-desc">
                        遊び心＋高品質設備により、暮らしの豊かさを味わって欲しい
                        <br />東京都足立区千住龍田町
                    </p>
                </div>
            </div> -->
        </div>
    </section>