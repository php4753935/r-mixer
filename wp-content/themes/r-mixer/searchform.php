<?php
    $http = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';

    $url = $http . '://' . $_SERVER['SERVER_NAME'];

    $strIdPurpose= isset($_POST['purpose']) && $_POST['purpose'] == true ? "&purpose=" .implode('C', $_POST['purpose']) : '';

    $strIdhouseConditional = isset($_POST['houseConditional']) && $_POST['houseConditional'] == true ? "&houseConditional=" .implode('C', $_POST['houseConditional']) : '';

    $strIdTypehouse = isset($_POST['typeHouse']) && $_POST['typeHouse'] == true ? "&typeHouse=" .implode('C', $_POST['typeHouse']) : '';

    $area = isset($_POST['area']) && $_POST['area'] != '' ? '&area=' .$_POST['area'] : '';

    $size = isset($_POST['size']) && $_POST['size'] != '' ? '&size=' . $_POST['size'] : '';

    $price = isset($_POST['price']) && $_POST['price'] != '' ? '&price=' .$_POST['price'] : '';
    
    header ("Location:" . $url . '/banhang/?s='.$strIdPurpose.$strIdhouseConditional.$strIdTypehouse.$area . $size .  $price);