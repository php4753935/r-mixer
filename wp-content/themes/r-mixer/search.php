<?php
/**
 * The template for displaying search results pages
 *
 */
get_header();
    $arrTaxoMyCustom = ['muc-dich' , 'yeu-cau' , 'loai-hinh' , 'khu-vuc' , 'do-rong' , 'khoang-gia'];

    $purpose = isset($_GET['purpose']) && $_GET['purpose'] != '' ? explode("C",$_GET['purpose']) : '';

    $houseConditional = isset($_GET['houseConditional']) && $_GET['houseConditional'] != '' ? explode("C",$_GET['houseConditional']) : '';

    $typeHouse = isset($_GET['typeHouse']) && $_GET['typeHouse'] != '' ? explode("C",$_GET['typeHouse']) : '';

    $area = isset($_GET['area']) && $_GET['area'] != ''? $_GET['area'] : '';

    $size = isset($_GET['size']) && $_GET['size'] != ''? $_GET['size'] : '';

    $price = isset($_GET['price']) && $_GET['price'] != '' ? $_GET['price'] : '';

    $paged =  get_query_var('paged') ? get_query_var('paged') : 1;
    $arr = array(
            'post_type' => 'post',
            'paged' => $paged,
            'max_num_pages' =>  10,
            'posts_per_page'=> 10,
            'tax_query' => array(
                'relation' => 'AND',
            ),
        );
        if(!empty($purpose)) {
            array_push($arr['tax_query'],            
                array (
                    'taxonomy'                      =>        'muc-dich',
                    'field'                         =>           'id',
                    'terms'                         =>        $purpose,
                )
            );
        }

        if(!empty($houseConditional)) {
            array_push($arr['tax_query'],
                array (
                    'taxonomy'                      =>          'yeu-cau',
                    'field'                         =>           'id',
                    'terms'                         =>        $houseConditional,
                    'operator'                      =>      'AND',
                ),
            );
        }

        if(!empty($typeHouse)) {
            array_push($arr['tax_query'],
                array(
                    'taxonomy'                      =>          'loai-hinh',
                    'field'                         =>           'id',
                    'terms'                         =>           $typeHouse,
                )
            );
        }

        if(!empty($area)) {
            array_push($arr['tax_query'],
                    array (
                        'taxonomy'                      =>          'khu-vuc',
                        'field'                         =>           'id',
                        'terms'                         =>        $area,
                    )
            );
        }

        if(!empty($size)) {
            array_push($arr['tax_query'],[
                ['relation' => 'OR',
                    array(
                        'taxonomy'                      =>          'do-rong',
                        'field'                         =>           'id',
                        'terms'                         =>          $size,
                    )
                ],
            ]);
        }

        if(!empty($size)) {
            array_push($arr['tax_query'],[
                ['relation' => 'OR',
                    array(
                        'taxonomy'                      =>          'do-rong',
                        'field'                         =>           'id',
                        'terms'                         =>          $size,
                    )
                ],
            ]);
        }
        
        if(!empty($price)) {
            array_push($arr['tax_query'],[
                ['relation' => 'OR',
                    array(
                        'taxonomy'                      =>          'khoang-gia',
                        'field'                         =>           'id',
                        'terms'                         =>          $price,
                    )
                ],
            ]);
        }
    $the_query = new WP_Query($arr);
    // dd($the_query );
?>

<?php
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
}
?>
<section id="search" style="margin:0">
    <div class="search-top">
        <div class="search-title d-flex">
            <i
                class="fa-solid fa-magnifying-glass"
                style="color: #937353"
            ></i>
            <h3>物件検索</h3>
        </div> 
        <form action="<?php bloginfo('template_url'); ?>/searchform.php" method="POST" class="search-content" id="filter">
            <?php
            $searchFields = array(
                array(
                    'label' => '[目的］',
                    'taxonomy' => 'muc-dich',
                    'name' => 'purpose',
                    'selectedValues' => $purpose
                ),
                array(
                    'label' => '[こだわり］',
                    'taxonomy' => 'yeu-cau',
                    'name' => 'houseConditional',
                    'selectedValues' => $houseConditional
                ),
                array(
                    'label' => '[形態］',
                    'taxonomy' => 'loai-hinh',
                    'name' => 'typeHouse',
                    'selectedValues' => $typeHouse
                )
            );

            foreach ($searchFields as $field) {
                $args = array(
                    'hide_empty' => 0,
                    'taxonomy' => $field['taxonomy']
                );
                $cates = get_categories($args);
                ?>
                <div class="search-box">
                    <h3><?php echo $field['label']; ?></h3>
                    <div class="search-info">
                        <?php foreach ($cates as $cate) { ?>
                            <div>
                                <input type="checkbox" name="<?php echo $field['name']; ?>[]" <?php echo !empty($field['selectedValues']) && in_array($cate->term_id, $field['selectedValues']) ? 'checked' : ''; ?> value="<?php echo $cate->term_id; ?>" id="name-<?php echo $cate->name; ?>" />
                                <label for="name-<?php echo $cate->name; ?>"><?php echo $cate->name; ?></label>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>

            <div class="search-box" style="margin-top: 10px;">
                <h3>[地域］</h3>
                <select name="area">
                    <option value="">指定しない"</option>
                    <?php
                        $args = array(
                            'hide_empty' => 0,
                            'taxonomy' => 'khu-vuc'
                        );
                        $cates = get_categories($args);
                        foreach ($cates as $cate) {
                    ?>
                        <option <?php if ($area == $cate->term_id) echo 'selected' ?> value="<?php echo $cate->term_id; ?>"><?php echo $cate->name; ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="search-box">
                <h3>[広さ］</h3>
                <select name="size">
                    <option value="">指定しない"</option>
                    <?php
                    $args = array(
                        'hide_empty' => 0,
                        'taxonomy' => 'do-rong'
                    );
                    $cates = get_categories($args);
                    foreach ($cates as $cate) {
                        ?>
                        <option <?php if ($size == $cate->term_id) echo 'selected' ?> value="<?php echo $cate->term_id; ?>"><?php echo $cate->name; ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="search-box">
                <h3>[価格帯（賃貸）］</h3>
                <select name="price">
                    <option value="">指定しない"</option>
                    <?php
                        $args = array(
                            'hide_empty' => 0,
                            'taxonomy' => 'khoang-gia'
                        );
                        $cates = get_categories($args);
                        foreach ($cates as $cate) {
                    ?>
                        <option <?php if ($price == $cate->term_id) echo 'selected' ?> value="<?php echo $cate->term_id; ?>"><?php echo $cate->name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <button type="submit" name="submit" class="search-btn">検索</button>  
        </form>
    </div>
    <div class="search-right">
        <div class="condition-search">
            <p>現在の検索結果</p>
            <?php
            $a = getTermAfterSearch($arrTaxoMyCustom);                
            foreach ($a as $item) {
                $html = '<div class="item">
                            <span>' . $item->name . '</span>
                            <i class="remove_params fas fa-times" data-id="' . $item->term_id . '"></i>
                        </div>';
                if (!empty($purpose) && in_array($item->term_id, $purpose)) {
                    echo $html;
                }
            
                if (!empty($houseConditional) && in_array($item->term_id, $houseConditional)) {
                    echo $html;
                }
                if (!empty($typeHouse) && in_array($item->term_id, $typeHouse)) {
                    echo $html;
                }
                if ($area == $item->term_id || $size == $item->term_id || $price == $item->term_id) {
                    echo $html;
                }
            }
            ?>
        </div>
        <div class="home-title">
            <p class="title">
                <?php
                    global $the_query;
                    echo $the_query->found_posts . '&nbsp;' . __('kết quả', 'theme-text-domain');
                ?>
            </p>
        </div>
        <ul class="product__item" style="margin-bottom: 10px;">
            <?php 
                while ( $the_query->have_posts() ) : $the_query->the_post();
                $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
            ?>
            <li>
                <a href="<?=the_permalink();?>" class="product__top">
                    <img src="<?php echo $featured_img_url; ?>" alt="ảnh4" />
                </a>
                <div class="product__info">
                    <h3><?php the_title(); ?></h3>
                    <?php the_excerpt(); ?>
                </div>
            </li>
            <?php endwhile; ?>
        </ul>
        <div class="pagination">
            <?php if(paginate_links() !='') {?>
                <div class="paginate_links">
                <?php
                    global $paged;
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    global $the_query;
                    echo paginate_links( array(
                        'prev_text'    => __('«'),
                        'next_text'    => __('»'),
                        'current' => max( 1, $paged ),
                        'total' => $the_query->max_num_pages,
                        ) );
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="search-map"> 
        <img src="<?=get_template_directory_uri()?>/assets/images/map.png" alt="map" />
        <h4>路線マップから探す！</h4>
    </div>
</section>
<?php get_footer(); ?>