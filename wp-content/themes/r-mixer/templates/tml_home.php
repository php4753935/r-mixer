<?php
/**
 * Template Name: Home
 * Author: R-mix
 */
 get_header();
 $img_dump = DIR_URI.'/assets/images/default.png';
//  $purpose = isset($_GET['purpose']) ? explode("C",$_GET['purpose']) : '';
?>


    
    <?php get_template_part('slider'); ?>
    
    <section id="link">
        <ul class="link-menu">
        <?php $getposts = new WP_query(); $getposts->query('post_status=publish&showposts=4&post_type=link&orderby=date&order=asc'); ?>
            <?php global $wp_query; $wp_query->in_the_loop = true; ?>
            <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
                <li class="link-item">
                    <a href="<?php the_permalink(); ?>" class="link-link"><?php the_title(); ?></a>
                </li>
            <?php endwhile; ?>     
            <!-- <li class="link-item">
            <a href="#" class="link-link">購入する</a>
            </li>
            <li class="link-item">
            <a href="#" class="link-link">売却する</a>
            </li>
            <li class="link-item">
            <a href="#" class="link-link">投資する</a>
            </li> -->
        </ul>
    </section>
    <section id="product">
        <div class="product-title">
            <i class="fa-sharp fa-solid fa-house" style="color: #8a6f49;font-size: 36px"></i>
            <p>物件の特徴で選べるお勧めタイプ</p>
        </div>
        <div class="container-sm">
            <ul class="home-item">
                <?php $getposts = new WP_query(); $getposts->query('post_status=publish&showposts=8&post_type=list-hot&orderby=date&order=asc'); ?>
                <?php global $wp_query; $wp_query->in_the_loop = true; ?>
                <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
                <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                <li class="home-list">
                    <h3><?php the_title(); ?></h3>
                    <a href="<?php the_permalink(); ?>" class="home-link"> 
                        <img src="<?php echo $featured_img_url; ?>" alt="item11">
                    </a>
                </li>
                <?php endwhile; ?>     
                <!-- <li class="home-list">
                    <h3>住むプラス</h3>
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r2.png" alt="item11">
                    </a>
                </li>
                <li class="home-list">
                    <h3>住むプラス</h3>
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r3.png" alt="item11">
                    </a>
                </li>
                <li class="home-list">
                    <h3>住むプラス</h3>
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r4.png" alt="item11">
                    </a>
                </li>
                <li class="home-list">
                    <h3 class="hideOn-mobile">住むプラス</h3>
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r5.png" alt="item11">
                    </a>
                </li>
                <li class="home-list">
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r6.png" alt="item11">
                    </a>
                </li>
                <li class="home-list hideOn-mobile">
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r7.png" alt="item11">
                    </a>
                </li>
                <li class="home-list hideOn-mobile ">
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r8.png" alt="item11">                
                    </a>
                </li> -->
            </ul>
            <div class="home__desc">
                <h2 class="desc__title">
                    私たちがつくるのは、<br>
                「こだわる、暮らし、自分らしく。」
                </h2>
                <p class="desc__desc">こだわり：proud　 <span>「こだわる」「好き」を大切にする。</span></p>
                <p class="desc__desc">空間：place　<span>「暮らし」「大切な毎日」を過ごす。</span></p>
                <p class="desc__desc">創り上げる：design　  <span>毎日「自分らしく」「心地よくありのまま」の自分でいる</span></p>   
            </div>
        </div>
    </section>
    <section id="search">
        <div class="search-top">
            <div class="search-title d-flex">
                <i
                    class="fa-solid fa-magnifying-glass"
                    style="color: #937353"
                ></i>
                <h3>物件検索</h3>
            </div> 
            <form action="<?php bloginfo('template_url'); ?>/searchform.php" method="POST" class="search-content">
                <input type="hidden" value="" name="s" id="s" class="searchform__input">
                <div class="search-box">
                    <h3>[目的］</h3>
                    <div class="search-info">
                        <?php $args = array( 
                            'hide_empty' => 0,
                            'taxonomy' => 'muc-dich',
                            ); 
                            $cates = get_categories( $args ); 
                            foreach ( $cates as $cate ) {  ?>
                                <div>
                                    <input type="checkbox" name="purpose[]" value="<?php echo $cate->term_id ?>" id="name-<?php echo $cate->name ?>" />
                                    <label for="name-<?php echo $cate->name ?>"><?php echo $cate->name ?></label>
                                </div>
                        <?php } ?>
                               
                        <!-- <div>
                            <input type="checkbox" checked name="" id="1" />
                            <label for="1">購入する</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="2" />
                            <label for="2">購入する</label>
                        </div> -->
                    </div>
                </div>
                <div class="search-box">
                    <h3>[こだわり］</h3>
                    <div class="search-info">
                        <?php $args = array( 
                            'hide_empty' => 0,
                            'taxonomy' => 'yeu-cau',
                            ); 
                            $cates = get_categories( $args ); 
                            foreach ( $cates as $cate ) {  ?>
                                <div>
                                    <input type="checkbox" name="houseConditional[]" value="<?php echo $cate->term_id ?>" class="call-ajax" id="name-<?php echo $cate->name ?>" />
                                    <label for="name-<?php echo $cate->name ?>"><?php echo $cate->name ?></label>
                                </div>
                        <?php } ?>


                        <!-- <div>
                            <input type="checkbox" checked name="" id="3" />
                            <label for="3">お家でお仕事</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="4" />
                            <label for="4">diyできる</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="5" />
                            <label for="5">ペットと暮らす</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="6" />
                            <label for="6">繋がりをつくる</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="7" />
                            <label for="7">design</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="8" />
                            <label for="8">駐車場</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="9" />
                            <label for="9">バイク</label>
                        </div> -->
                    </div>
                </div>
                <div class="search-box" style="margin-top: 10px;">
                    <h3>[形態］</h3>
                    <div class="search-info">
                        <?php $args = array( 
                            'hide_empty' => 0,
                            'taxonomy' => 'loai-hinh',
                            ); 
                            $cates = get_categories( $args ); 
                            foreach ( $cates as $cate ) {  ?>
                                <div>
                                    <input type="checkbox" value="<?php echo $cate->term_id ?>" name="typeHouse[]" id="name-<?php echo $cate->name ?>" />
                                    <label for="name-<?php echo $cate->name ?>"><?php echo $cate->name ?></label>
                                </div>
                        <?php } ?>
                        <!-- <div>
                            <input type="checkbox" checked name="" id="10" />
                            <label for="10">戸建</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="11" />
                            <label for="11">アパート</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="12" />
                            <label for="12">マンション</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="13" />
                            <label for="13">シェアハウス</label>
                        </div> -->
                    </div>
                </div>
                <div class="search-box" style="margin-top: 10px;">
                    <h3>[地域］</h3>
                    <select name="area">
                        <option value="" >指定しない"</option>
                        <?php $args = array( 
                            'hide_empty' => 0,
                            'taxonomy' => 'khu-vuc',
                            ); 
                            $cates = get_categories( $args ); 
                            foreach ( $cates as $cate ) {  ?>
                                <option value="<?php echo $cate->term_id; ?>"><?php echo $cate->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="search-box">
                    <h3>[広さ］</h3>
                    <select name="size">
                        <option value="" >指定しない"</option>
                        <?php $args = array( 
                            'hide_empty' => 0,
                            'taxonomy' => 'do-rong',
                            ); 
                            $cates = get_categories( $args ); 
                            foreach ( $cates as $cate ) {  ?>
                                <option value="<?php echo $cate->term_id; ?>"><?php echo $cate->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="search-box">
                    <h3>[価格帯（賃貸）］</h3>
                    <select name="price">
                        <option value="" >指定しない"</option>
                        <?php $args = array( 
                            'hide_empty' => 0,
                            'taxonomy' => 'khoang-gia',
                            ); 
                            $cates = get_categories( $args ); 
                            foreach ( $cates as $cate ) {  ?>
                                <option value="<?php echo $cate->term_id; ?>"><?php echo $cate->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <button type="submit" class="search-btn">検索</button>  
            </form>
        </div>
        <div class="search-right">
            <div class="home-title">
                <i class="fa-sharp fa-solid fa-house" style="color: #8a6f49"></i>
                <p>購入</p>
            </div>
            <ul class="product__item" style="margin-bottom: 10px;">
            <?php $getposts = new WP_query(); $getposts->query('post_status=publish&showposts=6&post_type=post&muc-dich=mua'); ?>
            <?php global $wp_query; $wp_query->in_the_loop = true; ?>
            <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                <li>
                    <a href="<?=the_permalink();?>" class="product__top">
                        <img src="<?php echo $featured_img_url; ?>" alt="ảnh4" />
                    </a>
                    <div class="product__info">
                        <h3><?php the_title(); ?></h3>
                        <?php the_excerpt(); ?>
                    </div>
                </li>
            <?php endwhile; ?>

            <!-- <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh4.png" alt="ảnh4" />
                </a>
                <div class="product__info">
                    <h3>北千住park view(シェアハウス)</h3>
                    <p class="product-desc">
                        遊び心＋高品質設備により、<br />
                        暮らしの豊かさを味わって欲しい
                    </p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh5.png" alt="ảnh5" />
                </a>
                <div class="product__info">
                    <h3>hagu組む 東戸塚【ペットと暮らせる】</h3>
                    <p class="product-desc">
                        多世代コミュニティ × 動物 × 自然。<br />「hagu組む」に住まう
                    </p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh6.png" alt="ảnh6" />
                </a>
                <div class="product__info">
                    <h3>東京コミュ +（シェアハウス）</h3>
                    <p class="product-desc">人とヒトのつながりを大切にした暮らし方</p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh4.png" alt="ảnh4" />
                </a>
                <div class="product__info">
                    <h3>北千住park view（シェアハウス）</h3>
                    <p class="product-desc">
                        遊び心＋高品質設備により、<br />
                        暮らしの豊かさを味わって欲しい
                    </p>
                </div>
            </li>
            <li class="hideOn-mobile">
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh5.png" alt="ảnh5" />
                </a>
                <div class="product__info">
                    <h3>hagu組む 東戸塚【ペットと暮らせる】</h3>
                    <p class="product-desc">
                        多世代コミュニティ × 動物 × 自然。<br />「hagu組む」に住まう
                    </p>
                </div>
            </li>
            <li class="hideOn-mobile">
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh6.png" alt="ảnh6" />
                </a>
                <div class="product__info">
                    <h3>東京コミュ +（シェアハウス）</h3>
                    <p class="product-desc">人とヒトのつながりを大切にした暮らし方</p>
                </div>
            </li> -->
            </ul>
            <a href="<?php get_template_part('post_type=post&muc-dich=mua') ?>" class="sub-desc">一覧はこちら</a>
            <div class="home-title">
                <i class="fa-sharp fa-solid fa-house" style="color: #8a6f49"></i>
                <p>借りる</p>
            </div>
            <ul class="product__item">
                <?php $getposts = new WP_query(); $getposts->query('post_status=publish&showposts=6&post_type=post&muc-dich=thue'); ?>
                <?php global $wp_query; $wp_query->in_the_loop = true; ?>
                <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
                <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
                    <li>
                        <a href="<?=the_permalink();?>" class="product__top">
                            <img src="<?php echo $featured_img_url; ?>" alt="ảnh4" />
                        </a>
                        <div class="product__info">
                            <h3><?php the_title(); ?></h3>
                            <?php the_excerpt(); ?>
                        </div>
                    </li>
                <?php endwhile; ?>
            <!-- <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh4.png" alt="ảnh4" />
                </a>
                <div class="product__info">
                    <h3>北千住park view(シェアハウス)</h3>
                    <p class="product-desc">
                        遊び心＋高品質設備により、<br />
                        暮らしの豊かさを味わって欲しい
                    </p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh5.png" alt="ảnh5" />
                </a>
                <div class="product__info">
                    <h3>hagu組む 東戸塚【ペットと暮らせる】</h3>
                    <p class="product-desc">
                        多世代コミュニティ × 動物 × 自然。<br />「hagu組む」に住まう
                    </p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh6.png" alt="ảnh6" />
                </a>
                <div class="product__info">
                    <h3>東京コミュ +（シェアハウス）</h3>
                    <p class="product-desc">人とヒトのつながりを大切にした暮らし方</p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh4.png" alt="ảnh4" />
                </a>
                <div class="product__info">
                    <h3>北千住park view（シェアハウス）</h3>
                    <p class="product-desc">
                        遊び心＋高品質設備により、<br />
                        暮らしの豊かさを味わって欲しい
                    </p>
                </div>
            </li>
            <li class="hideOn-mobile">
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh5.png" alt="ảnh5" />
                </a>
                <div class="product__info">
                    <h3>hagu組む 東戸塚【ペットと暮らせる】</h3>
                    <p class="product-desc">
                        多世代コミュニティ × 動物 × 自然。<br />「hagu組む」に住まう
                    </p>
                </div>
            </li>
            <li class="hideOn-mobile">
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh6.png" alt="ảnh6" />
                </a>
                <div class="product__info">
                    <h3>東京コミュ +（シェアハウス）</h3>
                    <p class="product-desc">人とヒトのつながりを大切にした暮らし方</p>
                </div>
            </li> -->
        </ul>
        <a href="#" class="sub-desc">一覧はこちら</a>
        </div>
        <div class="search-map">
            <img src="<?=get_template_directory_uri()?>/assets/images/map.png" alt="map" />
            <h4>路線マップから探す！</h4>
        </div>
        <div class="search__social">
            <div class="social">
                <span>「SNSはこちら」</span>
                <ul>
                    <li>
                    <a href="#">
                        <i
                        class="fa-brands fa-facebook fa-lg"
                        style="color: #6d6d6d"
                        ></i>
                    </a>
                    </li>
                    <li>
                    <a href="#">
                        <i
                        class="fa-brands fa-instagram fa-lg"
                        style="color: #6d6d6d"
                        ></i>
                    </a>
                    </li>
                    <li>
                    <a href="#">
                        <i
                        class="fa-brands fa-youtube fa-lg"
                        style="color: #6d6d6d"
                        ></i>
                    </a>
                    </li>
                    <li>
                    <a href="#">
                        <i class="fa-brands fa-tiktok fa-lg" style="color: #6d6d6d"></i>
                    </a>
                    </li>
                </ul>
            </div>
            <ul class="social-video">
            <li>
                <div>
                <img src="<?=get_template_directory_uri()?>/assets/images/vid-1.png" alt="ảnh4" />
                </div>
                <p class="product-desc">
                空き家×リノベーション <br />
                「hagu組む 根岸戸建て」
                </p>
            </li>
            <li>
                <div>
                <img src="<?=get_template_directory_uri()?>/assets/images/vid-2.png" alt="ảnh5" />
                </div>
                <div>
                <p class="product-desc">
                    シェアハウスで身に付く、<br />「丁寧な暮らし」の４つの…
                </p>
                </div>
            </li>
            <li class="hideOn-mobile">
                <div>
                <img src="<?=get_template_directory_uri()?>/assets/images/vid-3.png" alt="ảnh6" />
                </div>
                <p class="product-desc">a full life南品川</p>
            </li>
            </ul>
        </div>
    </section>
</main>
<?php get_footer(); ?>