<?php
get_header();
$img_dump = DIR_URI.'/assets/images/default.png';
?>
<main class="container-wraper">
    <section id="top-pic">
        <div class="container-sm">
            <div class="top-content">
                <div class="top-title">
                    <h2>私たちがつくるのは、<br>「こだわる、暮らし、自分らしく。」</h2>
                </div>
                <div class="top-logo">
                    <h3>R-mix</h3>
                </div>
            </div>
        </div>
    </section>
    
    <section id="slider">
        <div class="slider-content">
            <div class="image-item">
                <div class="image">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh1.png" alt="ảnh1" />
                </div>
                <div class="con">
                    <h3 class="image-title">bicolo 尾山台【ペットと暮らせる</h3>
                    <p class="image-desc">対比的な２つの場所 <br />世田谷区等々力二丁目</p>
                </div>
            </div>
            <div class="image-item">
                <div class="image">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh2.png" alt="ảnh2" />
                </div>
                <div class="con">
                    <h3 class="image-title">辰巳アパートメントハウス</h3>
                    <p class="image-desc">
                        design by 伊藤博之建築設計事務所 <br />東京都江東区門前仲町1丁目
                    </p>
                </div>
            </div>
            <div class="image-item">
                <div class="image">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh3.png" alt="ảnh3" />
                </div>
                <div class="con">
                    <h3 class="image-title">北千住park view（シェアハウス）</h3>
                    <p class="image-desc">
                        遊び心＋高品質設備により、暮らしの豊かさを味わって欲しい
                        <br />東京都足立区千住龍田町
                    </p>
                </div>
            </div>
            <div class="image-item">
                <div class="image">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh3.png" alt="ảnh3" />
                </div>
                <div class="con">
                    <h3 class="image-title">北千住park view（シェアハウス）</h3>
                    <p class="image-desc">
                        遊び心＋高品質設備により、暮らしの豊かさを味わって欲しい
                        <br />東京都足立区千住龍田町
                    </p>
                </div>
            </div>
            <div class="image-item">
                <div class="image">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh3.png" alt="ảnh3" />
                </div>
                <div class="con">
                    <h3 class="image-title">北千住park view（シェアハウス）</h3>
                    <p class="image-desc">
                        遊び心＋高品質設備により、暮らしの豊かさを味わって欲しい
                        <br />東京都足立区千住龍田町
                    </p>
                </div>
            </div>
        </div>
    </section>
    
    <section id="link">
        <ul class="link-menu">
            <li class="link-item">
            <a href="#" class="link-link">借りる</a>
            </li>
            <li class="link-item">
            <a href="#" class="link-link">購入する</a>
            </li>
            <li class="link-item">
            <a href="#" class="link-link">売却する</a>
            </li>
            <li class="link-item">
            <a href="#" class="link-link">投資する</a>
            </li>
        </ul>
    </section>
    <section id="product">
        <div class="product-title">
            <i class="fa-sharp fa-solid fa-house" style="color: #8a6f49;font-size: 36px"></i>
            <p>物件の特徴で選べるお勧めタイプ</p>
        </div>
        <div class="container-sm">
            <ul class="home-item">
                <li class="home-list">
                    <h3>住むプラス</h3>
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r1.png" alt="item11">
                    </a>
                </li>
                <li class="home-list">
                    <h3>住むプラス</h3>
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r2.png" alt="item11">
                    </a>
                </li>
                <li class="home-list">
                    <h3>住むプラス</h3>
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r3.png" alt="item11">
                    </a>
                </li>
                <li class="home-list">
                    <h3>住むプラス</h3>
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r4.png" alt="item11">
                    </a>
                </li>
                <li class="home-list">
                    <h3 class="hideOn-mobile">住むプラス</h3>
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r5.png" alt="item11">
                    </a>
                </li>
                <li class="home-list">
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r6.png" alt="item11">
                    </a>
                </li>
                <li class="home-list hideOn-mobile">
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r7.png" alt="item11">
                    </a>
                </li>
                <li class="home-list hideOn-mobile ">
                    <a href="#" class="home-link"> 
                        <img src="<?=get_template_directory_uri()?>/assets/images/img-r8.png" alt="item11">                
                    </a>
                </li>
            </ul>
            <div class="home__desc">
                <h2 class="desc__title">
                    私たちがつくるのは、<br>
                「こだわる、暮らし、自分らしく。」
                </h2>
                <p class="desc__desc">こだわり：proud　 <span>「こだわる」「好き」を大切にする。</span></p>
                <p class="desc__desc">空間：place　<span>「暮らし」「大切な毎日」を過ごす。</span></p>
                <p class="desc__desc">創り上げる：design　  <span>毎日「自分らしく」「心地よくありのまま」の自分でいる</span></p>   
            </div>
        </div>
    </section>
    <section id="search">
        <div class="search-top">
            <div class="search-title d-flex">
                <i
                    class="fa-solid fa-magnifying-glass"
                    style="color: #937353"
                ></i>
                <h3>物件検索</h3>
            </div>
            <form action="#" method="post" class="search-content">
                <div class="search-box">
                    <h3>[目的］</h3>
                    <div class="search-info">
                        <div>
                            <input type="checkbox" checked name="" id="1" />
                            <label for="1">購入する</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="2" />
                            <label for="2">購入する</label>
                        </div>
                    </div>
                </div>
                <div class="search-box">
                    <h3>[こだわり］</h3>
                    <div class="search-info">
                        <div>
                            <input type="checkbox" checked name="" id="3" />
                            <label for="3">お家でお仕事</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="4" />
                            <label for="4">diyできる</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="5" />
                            <label for="5">ペットと暮らす</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="6" />
                            <label for="6">繋がりをつくる</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="7" />
                            <label for="7">design</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="8" />
                            <label for="8">駐車場</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="9" />
                            <label for="9">バイク</label>
                        </div>
                    </div>
                </div>
                <div class="search-box" style="margin-top: 10px;">
                    <h3>[形態］</h3>
                    <div class="search-info">
                        <div>
                            <input type="checkbox" checked name="" id="10" />
                            <label for="10">戸建</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="11" />
                            <label for="11">アパート</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="12" />
                            <label for="12">マンション</label>
                        </div>
                        <div>
                            <input type="checkbox" name="" id="13" />
                            <label for="13">シェアハウス</label>
                        </div>
                    </div>
                </div>
                <div class="search-box" style="margin-top: 10px;">
                    <h3>[地域］</h3>
                    <select name="" id="">
                        <option value="1">指定しない"</option>
                    </select>
                </div>
                <div class="search-box">
                    <h3>[広さ］</h3>
                    <select name="" id="">
                        <option value="1">指定しない"</option>
                    </select>
                </div>
                <div class="search-box">
                    <h3>[価格帯（賃貸）］</h3>
                    <select name="" id="">
                        <option value="1">指定しない"</option>
                    </select>
                </div>
                <button type="submit" class="search-btn">検索</button>  
            </form>
        </div>
        <div class="search-right">
            <div class="home-title">
                <i class="fa-sharp fa-solid fa-house" style="color: #8a6f49"></i>
                <p>購入</p>
            </div>
            <ul class="product__item" style="margin-bottom: 10px;">
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh4.png" alt="ảnh4" />
                </a>
                <div class="product__info">
                    <h3>北千住park view(シェアハウス)</h3>
                    <p class="product-desc">
                        遊び心＋高品質設備により、<br />
                        暮らしの豊かさを味わって欲しい
                    </p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh5.png" alt="ảnh5" />
                </a>
                <div class="product__info">
                    <h3>hagu組む 東戸塚【ペットと暮らせる】</h3>
                    <p class="product-desc">
                        多世代コミュニティ × 動物 × 自然。<br />「hagu組む」に住まう
                    </p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh6.png" alt="ảnh6" />
                </a>
                <div class="product__info">
                    <h3>東京コミュ +（シェアハウス）</h3>
                    <p class="product-desc">人とヒトのつながりを大切にした暮らし方</p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh4.png" alt="ảnh4" />
                </a>
                <div class="product__info">
                    <h3>北千住park view（シェアハウス）</h3>
                    <p class="product-desc">
                        遊び心＋高品質設備により、<br />
                        暮らしの豊かさを味わって欲しい
                    </p>
                </div>
            </li>
            <li class="hideOn-mobile">
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh5.png" alt="ảnh5" />
                </a>
                <div class="product__info">
                    <h3>hagu組む 東戸塚【ペットと暮らせる】</h3>
                    <p class="product-desc">
                        多世代コミュニティ × 動物 × 自然。<br />「hagu組む」に住まう
                    </p>
                </div>
            </li>
            <li class="hideOn-mobile">
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh6.png" alt="ảnh6" />
                </a>
                <div class="product__info">
                    <h3>東京コミュ +（シェアハウス）</h3>
                    <p class="product-desc">人とヒトのつながりを大切にした暮らし方</p>
                </div>
            </li>
            <a href="#" class="sub-desc">一覧はこちら</a>
            </ul>
            <div class="home-title">
                <i class="fa-sharp fa-solid fa-house" style="color: #8a6f49"></i>
                <p>借りる</p>
            </div>
            <ul class="product__item">
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh4.png" alt="ảnh4" />
                </a>
                <div class="product__info">
                    <h3>北千住park view(シェアハウス)</h3>
                    <p class="product-desc">
                        遊び心＋高品質設備により、<br />
                        暮らしの豊かさを味わって欲しい
                    </p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh5.png" alt="ảnh5" />
                </a>
                <div class="product__info">
                    <h3>hagu組む 東戸塚【ペットと暮らせる】</h3>
                    <p class="product-desc">
                        多世代コミュニティ × 動物 × 自然。<br />「hagu組む」に住まう
                    </p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh6.png" alt="ảnh6" />
                </a>
                <div class="product__info">
                    <h3>東京コミュ +（シェアハウス）</h3>
                    <p class="product-desc">人とヒトのつながりを大切にした暮らし方</p>
                </div>
            </li>
            <li>
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh4.png" alt="ảnh4" />
                </a>
                <div class="product__info">
                    <h3>北千住park view（シェアハウス）</h3>
                    <p class="product-desc">
                        遊び心＋高品質設備により、<br />
                        暮らしの豊かさを味わって欲しい
                    </p>
                </div>
            </li>
            <li class="hideOn-mobile">
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh5.png" alt="ảnh5" />
                </a>
                <div class="product__info">
                    <h3>hagu組む 東戸塚【ペットと暮らせる】</h3>
                    <p class="product-desc">
                        多世代コミュニティ × 動物 × 自然。<br />「hagu組む」に住まう
                    </p>
                </div>
            </li>
            <li class="hideOn-mobile">
                <a href="#" class="product__top">
                    <img src="<?=get_template_directory_uri()?>/assets/images/ảnh6.png" alt="ảnh6" />
                </a>
                <div class="product__info">
                    <h3>東京コミュ +（シェアハウス）</h3>
                    <p class="product-desc">人とヒトのつながりを大切にした暮らし方</p>
                </div>
            </li>
            <a href="#" class="sub-desc">一覧はこちら</a>
            </ul>
        </div>
        <div class="search-map">
            <img src="<?=get_template_directory_uri()?>/assets/images/map.png" alt="map" />
            <h4>路線マップから探す！</h4>
        </div>
        <div class="search__social">
            <div class="social">
                <span>「SNSはこちら」</span>
                <ul>
                    <li>
                    <a href="#">
                        <i
                        class="fa-brands fa-facebook fa-lg"
                        style="color: #6d6d6d"
                        ></i>
                    </a>
                    </li>
                    <li>
                    <a href="#">
                        <i
                        class="fa-brands fa-instagram fa-lg"
                        style="color: #6d6d6d"
                        ></i>
                    </a>
                    </li>
                    <li>
                    <a href="#">
                        <i
                        class="fa-brands fa-youtube fa-lg"
                        style="color: #6d6d6d"
                        ></i>
                    </a>
                    </li>
                    <li>
                    <a href="#">
                        <i class="fa-brands fa-tiktok fa-lg" style="color: #6d6d6d"></i>
                    </a>
                    </li>
                </ul>
            </div>
            <ul class="social-video">
            <li>
                <div>
                <img src="<?=get_template_directory_uri()?>/assets/images/vid-1.png" alt="ảnh4" />
                </div>
                <p class="product-desc">
                空き家×リノベーション <br />
                「hagu組む 根岸戸建て」
                </p>
            </li>
            <li>
                <div>
                <img src="<?=get_template_directory_uri()?>/assets/images/vid-2.png" alt="ảnh5" />
                </div>
                <div>
                <p class="product-desc">
                    シェアハウスで身に付く、<br />「丁寧な暮らし」の４つの…
                </p>
                </div>
            </li>
            <li class="hideOn-mobile">
                <div>
                <img src="<?=get_template_directory_uri()?>/assets/images/vid-3.png" alt="ảnh6" />
                </div>
                <p class="product-desc">a full life南品川</p>
            </li>
            </ul>
        </div>
    </section>
</main>
<?php get_footer(); ?>
