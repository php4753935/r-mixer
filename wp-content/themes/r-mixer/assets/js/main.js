$(document).ready(function () {
    $(".slider-content").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      draggable: false,
      autoplay: false,
      autoplayspeed: 2000,
      margin: 10,
      arrows: true,
      prevArrow: `<button type='button' class='slick-prev slick-arrow'><i class="fa-solid fa-angle-left" style="color: #ffffff;"></i></button>`,
      nextArrow: `<button type='button' class='slick-next slick-arrow'><i class="fa-solid fa-angle-right" style="color: #ffffff;"></i></button>`,
      dots: true,
      responsive: [
        {        
          breakpoint: 980,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 749.98,
          settings: {
            slidesToShow: 1,
          },
        }
      ],
    });
  });
  
  const hamburger =  document.querySelector('.hamburger');
  const menu =  document.querySelector('.nav');
  hamburger.addEventListener('click', function(){
      menu.classList.toggle('open');
  })
  const remove = document.querySelectorAll('.remove_params')
  for (var i of remove)
  i.addEventListener('click', function() {
  const dataId = $(this).attr("data-id")
  const originalURL = document.location.href;
  removeIdFromURL(dataId, originalURL);
  const checkbox = document.querySelectorAll('input[checked]')
  for(let z of checkbox)
    if(z.getAttribute('value') === dataId && z.checked == true)
    {
      z.checked = false;
    }
})

function removeIdFromURL(key, url) {
  const urlParams = url.split("&");
  const newURL = urlParams.map((param, index) => {
      if (!index) {
          return param;
      }
      const [keyParam, valueParam] = param.split("=");
      const ids = valueParam.split("C");
      const idsOK = ids.filter((id) => id != key);
      if (ids.length === idsOK.length) {
          return param;
      } else {
          const newValueParam = idsOK.join("C");
          return `${keyParam}=${newValueParam}`;
      }
  });
      updateUrl = newURL.join("&");
      window.location.href = updateUrl;
}


  