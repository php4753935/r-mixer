<?php
if (!function_exists('template_scripts')) {
    function template_scripts()
    {
        // CSS
        wp_enqueue_style( 'slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
//        wp_enqueue_style('animate-css', DIR_URI . '/node_modules/wow.js/css/libs/animate.css', array(), '4.1.1');
        wp_enqueue_style('main-css', DIR_URI . './assets/css/main.css?r=' . rand(100, 999), array(), THEME_VER);
        wp_enqueue_style('icon-font',  DIR_URI . './assets/libs/fontawesome-free-6.4.2-web/css/all.min.css');

        // JS
        wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.7.1.min.js');
        wp_enqueue_script('slick',  'https://code.jquery.com/jquery-1.11.0.min.js');
        wp_enqueue_script('slick2',  'https://code.jquery.com/jquery-migrate-1.2.1.min.js');
        wp_enqueue_script('slick3',  'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js');
        wp_enqueue_script('main-com-js',  DIR_URI . '/assets/js/main.js', array(), THEME_VER, true);
        wp_localize_script( 'main-js', 'global_variables',
            array(
                'ajaxurl' => admin_url( 'admin-ajax.php' ),
                'is_home' => is_front_page(),
                'home_url' => home_url(),
                'home_section' => @$_REQUEST['home-section'],
            )
        );
    }
}

add_action('wp_enqueue_scripts', 'template_scripts');