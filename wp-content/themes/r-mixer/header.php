<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>R-Mix</title>
    <?php wp_head(); ?>
</head>
<body>
<header class="header">
    <div class="container-wraper">
        <div class="header-content">
            <div class="logo">
                <a href="<?php bloginfo('url') ?>">
                <h1>
                    <img src="<?php bloginfo('template_directory') ?>/assets/images/logoR.png" alt="R-bank"/>
                    リノベーションから <br />新しい住まいの創造を
                </h1>
                </a>
            </div>
            <div class="hamburger">
                <i class="fa-solid fa-bars fa-lg" style="color: #000000;"></i>
            </div>
            <div class="nav">
                <?php
                    wp_nav_menu(array(
                    'theme_location' => 'primary',
                    'container' => 'false',
                    'menu_class' => 'list'
                    ));
                ?>
            </div>
        </div>
    </div>
</header>

<main class="container-wraper">
    <section id="top-pic">
        <div class="container-sm">
            <div class="top-content">
                <div class="top-title">
                    <h2>私たちがつくるのは、<br>「こだわる、暮らし、自分らしく。」</h2>
                </div>
                <div class="top-logo">
                    <h3>R-mix</h3>
                </div>
            </div>
        </div>
    </section>

