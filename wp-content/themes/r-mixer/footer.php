<footer>
        <div class="container-wraper">
            <div class="footer-top">
                <div class="footer-left">
                    <i class="fa-solid fa-play"></i>
                    <span>プライバシーポリシー</span>
                </div>
                <div class="footer-right">
                    <a href="<?php bloginfo('url') ?>"><img src="<?=get_template_directory_uri()?>/assets/images/logoR.png" alt="" /></a>
                </div>
            </div>
        </div>
        <div class="hideOn-PC">
            <div class="search-title">
                <i
                    class="fa-solid fa-magnifying-glass fa-lg"
                    style="color: #6d6d6d"
                ></i>
                <h3>物件検索</h3>
            </div>
            <div class="contact">
                <div class="phone">
                    <i class="fa-solid fa-phone fa-lg" style="color: #ffffff;"></i>
                    <span>お電話で <br> お問い合わせ</span>
                </div>
                <div class="email">
                    <i class="fa-regular fa-envelope fa-lg" style="color: #8e7457;"></i>
                    <span>メールで <br>  お問い合わせ</span>
                </div>
            </div>
        </div>
        <div class="footer-btm">
            <span>Copyright（C）2022 Rバンク All Rights Reserved.</span>
        </div>
</footer>

</body>
</html>
<?php wp_footer(); ?>