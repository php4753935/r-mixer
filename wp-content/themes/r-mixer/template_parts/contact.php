<div class="redirect">
    <p class="redirect-title"><?php _e("Liên hệ với chúng tôi", "vietnewday"); ?></p>
    <p class="note"><?php _e("Chúng tôi sẽ phản hồi bạn sẽ phản hổi sớm nhất", "vietnewday"); ?></p>
    <?php
    $args = array(
        'name'        => 'lien-he',
        'post_type'   => 'page',
        'post_status' => 'publish',
        'numberposts' => 1
    );
    $contact = get_posts($args);
    $contact_id = $contact[0]->ID;
    $contact_id_lang = pll_get_post($contact_id, pll_current_language('slug'));
    $contact_post = get_post($contact_id_lang);
    ?>
    <a class="btn-contact" href="<?= get_page_link($contact_id_lang) ?>"><span><?= $contact_post->post_title ?></span><i class="fas fa-paper-plane"></i></a>
</div>