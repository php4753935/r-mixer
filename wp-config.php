<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'banhang' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'U.M8DK4@b.^oQVPKGe25_1hA7jVdeL-@ bn/@|pSK=dX/BrI^RI1YYnySi*/!U_k' );
define( 'SECURE_AUTH_KEY',  '7MwG]Bs*O.P@)SC1=CfEC~knK2`k$|=||eZ{lLuoBhpkz(_4z 0&^9HH-zl5L|~L' );
define( 'LOGGED_IN_KEY',    'FeRDklQ{5jvZfQ2S)euioPWNcJF<d/Q2%&Y5#o]n|$Do54D.6xg(PK6K1eo(mo)i' );
define( 'NONCE_KEY',        '<$#jvg`-~sL_!81z=Um<S<li#vvS%s)W>( })|xN[B?_^`vKRMRq~M?%Vxg>]?xE' );
define( 'AUTH_SALT',        'i.Wh&;)#V10OSf!xCp%d`tBE:jKTAQ,I8197>4E4+4VrH<(G?Ar|EX8G,A*w^Q/+' );
define( 'SECURE_AUTH_SALT', 'lsRMGI]$id1X7cRhK*|T8ub@M[wWCir&d}h-{(zpX}8>SRVww;Tt&r&Pz)XA%V:R' );
define( 'LOGGED_IN_SALT',   'QYbeZ_<mrv4pCE~K8D/T o{{6lGW~%Sw_+7x?MliWTE!X.73{O+MdxT4Sd21^04g' );
define( 'NONCE_SALT',       '|zChx4BCK98UW-ovbi98426(fd%[J<E?]c+jjH;7z]7b|(hyfw^h>t6!;1WX92D1' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'oto_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
